import React from 'react'
import './first.css'

function First() {
    return (
    <div className="root">
      <div className="app">
        <span className="app_title">
          <span className="title_question">
            In the Past Three Months, Did You experience Loneliness?
          </span>
          <p className="title_questionlistitems">View Question List</p>
        </span>
      </div>
      <div className="app">
        <div className="app_box1">
          <button className="button1">Add a question</button>
        </div>

        <div className="app_box2">
          <div className="box2_question">
            In the Past Three Months, Did You experience Loneliness?
          </div>
        <br />
          <p>
            <button className="box2_button1">View as Data</button>
            <button className="box2_button2">View as Chart</button>
          </p>
          <p className="yespercent">63.32%</p>
          <p>Yes</p>
          <p className="nopercent"> 36.68%</p>
          <p>No</p>
          <div className="app_box4">
          <button className="button1">Download Data</button>
        </div>
        </div>

        <div className="app_box3">
          <button className="button1">Add a question</button>
        </div>
      </div>
        {/* {} */}

        <div className="app">
        <span className="app_title">
          <span className="title_question">
            In the Past Three Months, Did You experience Loneliness?
          </span>
          <p className="title_questionlistitems">View Question List</p>
        </span>
      </div>
      <div className="app">
        <div className="app_box1">
          <button className="button1">Add a question</button>
        </div>

        <div className="app_box2">
          <div className="box2_question">
            In the Past Three Months, Did You experience Loneliness?
          </div>
        <br />
          <p>
            <button className="box2_button1">View as Data</button>
            <button className="box2_button2">View as Chart</button>
          </p>
          <p className="yespercent">63.32%</p>
          <p>Yes</p>
          <p className="nopercent"> 36.68%</p>
          <p>No</p>
          <div className="app_box4">
          <button className="button1">Download Data</button>
        </div>
        </div>

        <div className="app_box3">
          <button className="button1">Add a question</button>
        </div>
      </div>
    </div>
    )
}

export default First
