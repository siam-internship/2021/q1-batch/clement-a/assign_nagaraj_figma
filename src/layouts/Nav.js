import React from 'react'
import './nav.css'
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";

function Nav() {
    return (
      <div>
          <div className="nav">
              <div className="nav_right_title">
                  <h5>Profile Builder</h5>
              </div>

              <div className="nav_left_title">
                  <button className="button"> Clear Filters </button>
              </div>
          </div>

        <div className="nav">
          <div className="nav_right">
            <h5>Region</h5>
          </div>

          <div className="nav_left">
            <h3>
              <KeyboardArrowDownIcon />
            </h3>
          </div>
        </div>

        <div className="nav">
          <div className="nav_right">
            <h5>Region</h5>
          </div>

          <div className="nav_left">
            <h3>
              <KeyboardArrowDownIcon />
            </h3>
          </div>
        </div>

        <div className="nav">
          <div className="nav_right">
            <h5>Region</h5>
          </div>

          <div className="nav_left">
            <h3>
              <KeyboardArrowDownIcon />
            </h3>
          </div>
        </div>
      </div>
    )
}



export default Nav
