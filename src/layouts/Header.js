import './header.css'

import React, { Component } from 'react'

export class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {
             show:true,
             scrollPos:0
        }
    }
    componentWillUnmount() {
        window.removeEventListener('scroll',this.handleScroll)
    }
    componentDidMount() {
        window.addEventListener('scroll',this.handleScroll)
    }
    handleScroll = () => {
        // console.log(document.body.getBoundingClientRect());
        this.setState({
            scrollPos:document.body.getBoundingClientRect().top,
            show:document.body.getBoundingClientRect().top > this.state.scrollPos
        });
    }
    
    render() {
        console.log(this.state);
        return (
            <nav className={this.state.show ? 'active' :'hidden'}>
            <div className="header">
                 {/* images */}
             <div className='header__text'>
                <ul>
                     <li><a href="#">Home</a></li>
                    <li><a href="#">Data Explorer</a></li>
                     <li><a href="#">Report & Resources</a></li>
                     <li><a href="#">Do you know Gen Z?</a></li>
                     <li><a href="#">Share</a></li>
                 </ul>
                
             </div>
         </div>
         </nav>
        )
    }
}

export default Header

// function Header() {
//     return (
//         <div className="header">
//                 {/* images */}
//             <div className='header__text'>
//                 <ul>
//                     <li><a href="#">Home</a></li>
//                     <li><a href="#">Data Explorer</a></li>
//                     <li><a href="#">Report & Resources</a></li>
//                     <li><a href="#">Do you know Gen Z?</a></li>
//                     <li><a href="#">Share</a></li>
//                 </ul>
                
//             </div>
//         </div>
//     )
// }

// export default Header
