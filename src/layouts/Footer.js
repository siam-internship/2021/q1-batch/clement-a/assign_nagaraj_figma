import React from 'react'
import './footer.css'
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import TwitterIcon from '@material-ui/icons/Twitter';
import PinterestIcon from '@material-ui/icons/Pinterest';
import RedditIcon from '@material-ui/icons/Reddit';

function Footer() {
    return (
        <footer className="footer">
                <div className="Page__Footer__icons">
                    <ul>
                        <li><a href="#"></a><FacebookIcon /> </li>
                        <li><a href="#"></a><InstagramIcon /> </li>
                        <li><a href="#"></a><TwitterIcon /> </li>
                        <li><a href="#"></a><PinterestIcon/> </li>
                        <li><a href="#"></a><RedditIcon /> </li>
                    </ul>
                </div>
                <div className="Page__Footer__text">
                    <ul>
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Contact us</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Privacy policy</a></li>
                    </ul>
                </div>
            </footer>
    )
}

export default Footer
